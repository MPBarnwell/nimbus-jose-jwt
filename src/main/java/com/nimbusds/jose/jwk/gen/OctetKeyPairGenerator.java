/*
 * nimbus-jose-jwt
 *
 * Copyright 2012-2018, Connect2id Ltd and contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */

package com.nimbusds.jose.jwk.gen;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.crypto.bc.BouncyCastleProviderSingleton;
import com.nimbusds.jose.jwk.Curve;
import com.nimbusds.jose.jwk.OctetKeyPair;
import com.nimbusds.jose.util.Base64URL;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.jcajce.provider.asymmetric.EdEC;
import org.bouncycastle.jcajce.provider.asymmetric.edec.BCEdDSAPrivateKey;
import org.bouncycastle.jcajce.provider.asymmetric.edec.KeyPairGeneratorSpi;

import java.io.IOException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;


/**
 * Octet Key Pair (OKP) JSON Web Key (JWK) generator.
 *
 * <p>Supported curves:
 *
 * <ul>
 *     <li>{@link Curve#X25519 X25519}
 *     <li>{@link Curve#Ed25519 Ed25519}
 * </ul>
 *
 * @author Tim McLean
 * @version 2018-07-18
 */
public class OctetKeyPairGenerator extends JWKGenerator<OctetKeyPair> {


	/**
	 * The curve.
	 */
	private final Curve crv;


	/**
	 * The supported values for the "crv" property.
	 */
	public static final Set<Curve> SUPPORTED_CURVES;

	/**
	 * The secure random generator to use, {@code null} to use the default
	 * one.
	 */
	private SecureRandom secureRandom;

	static {
		Set<Curve> curves = new LinkedHashSet<>();
		curves.add(Curve.X25519);
		curves.add(Curve.Ed25519);
		SUPPORTED_CURVES = Collections.unmodifiableSet(curves);
	}


	/**
	 * Creates a new OctetKeyPair JWK generator.
	 *
	 * @param crv The curve. Must not be {@code null}.
	 */
	public OctetKeyPairGenerator(final Curve crv) {

		if (crv == null) {
			throw new IllegalArgumentException("The curve must not be null");
		}

		if (! SUPPORTED_CURVES.contains(crv)) {
			throw new IllegalArgumentException("Curve not supported for OKP generation");
		}

		this.crv = crv;
	}

	/**
	 * Sets the secure random generator to use.
	 *
	 * @param secureRandom The secure random generator to use, {@code null}
	 *                     to use the default one.
	 *
	 * @return This generator.
	 */
	public OctetKeyPairGenerator secureRandom(final SecureRandom secureRandom) {

		this.secureRandom = secureRandom;
		return this;
	}
	
	@Override
	public OctetKeyPair generate()
		throws JOSEException {

		Ed25519PrivateKeyParameters keyPair = new Ed25519PrivateKeyParameters(
				this.secureRandom != null ? secureRandom : new SecureRandom());

		final Base64URL privateKey = Base64URL.encode(keyPair.getEncoded());
		final Base64URL publicKey = Base64URL.encode(keyPair.generatePublicKey().getEncoded());

		OctetKeyPair.Builder builder = new OctetKeyPair.Builder(crv, publicKey)
				.d(privateKey)
				.keyUse(use)
				.keyOperations(ops)
				.algorithm(alg);

		if (x5tKid) {
			builder.keyIDFromThumbprint();
		} else {
			builder.keyID(kid);
		}

		return builder.build();
	}
}

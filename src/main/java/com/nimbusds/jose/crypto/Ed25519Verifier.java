/*
 * nimbus-jose-jwt
 *
 * Copyright 2012-2018, Connect2id Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */

package com.nimbusds.jose.crypto;


import com.nimbusds.jose.CriticalHeaderParamsAware;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.bc.BouncyCastleProviderSingleton;
import com.nimbusds.jose.crypto.impl.CriticalHeaderParamsDeferral;
import com.nimbusds.jose.crypto.impl.EdDSAProvider;
import com.nimbusds.jose.jwk.Curve;
import com.nimbusds.jose.jwk.OctetKeyPair;
import com.nimbusds.jose.util.Base64URL;
import net.jcip.annotations.ThreadSafe;
import org.bouncycastle.jcajce.interfaces.EdDSAKey;
import org.bouncycastle.jcajce.provider.asymmetric.edec.BCEdDSAPublicKey;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Set;


/**
 * Ed25519 verifier of {@link com.nimbusds.jose.JWSObject JWS objects}.
 * Expects a public {@link OctetKeyPair} with {@code "crv"} Ed25519.
 * Uses the Edwards-curve Digital Signature Algorithm (EdDSA).
 *
 * <p>See <a href="https://tools.ietf.org/html/rfc8037">RFC 8037</a>
 * for more information.
 *
 * <p>This class is thread-safe.
 *
 * <p>Supports the following algorithm:
 *
 * <ul>
 *     <li>{@link com.nimbusds.jose.JWSAlgorithm#EdDSA}
 * </ul>
 *
 * <p>with the following curve:
 *
 * <ul>
 *     <li>{@link com.nimbusds.jose.jwk.Curve#Ed25519}
 * </ul>
 *
 * @author Tim McLean
 * @version 2018-07-11
 */
@ThreadSafe
public class Ed25519Verifier extends EdDSAProvider implements JWSVerifier, CriticalHeaderParamsAware {


	private final CriticalHeaderParamsDeferral critPolicy = new CriticalHeaderParamsDeferral();


	private final PublicKey publicKey;

	/**
	 * Creates a new Edwards Digital Signature Algorithm (EdDSA)  verifier.
	 *
	 * @param publicKey The public Ed25519 key. Must not be {@code null}.
	 *
	 * @throws JOSEException If the elliptic curve of key is not supported.
	 */
	public Ed25519Verifier(final PublicKey publicKey)
			throws JOSEException {

		super();

		if (!(publicKey instanceof BCEdDSAPublicKey)) {
			throw new JOSEException("Ed25519Verifier requires a public key, use OctetKeyPair.toPublicJWK()");
		}

		if (!Curve.Ed25519.getName().equals(publicKey.getAlgorithm())) {
			throw new JOSEException("Ed25519Verifier only supports OctetKeyPairs with crv=Ed25519");
		}

		this.publicKey = publicKey;
	}

	/**
	 * Creates a new Ed25519 verifier.
	 *
	 * @param publicKey The public Ed25519 key. Must not be {@code null}.
	 *
	 * @throws JOSEException If the key subtype is not supported
	 */
	public Ed25519Verifier(final OctetKeyPair publicKey)
		throws JOSEException {

		this(publicKey, null);
	}


	/**
	 * Creates a Ed25519 verifier.
	 *
	 * @param publicKey      The public Ed25519 key. Must not be {@code null}.
	 * @param defCritHeaders The names of the critical header parameters
	 *                       that are deferred to the application for
	 *                       processing, empty set or {@code null} if none.
	 *
	 * @throws JOSEException If the key subtype is not supported.
	 */
	public Ed25519Verifier(final OctetKeyPair publicKey, final Set<String> defCritHeaders)
		throws JOSEException {

		super();

		if (! Curve.Ed25519.equals(publicKey.getCurve())) {
			throw new JOSEException("Ed25519Verifier only supports OctetKeyPairs with crv=Ed25519");
		}

		if (publicKey.isPrivate()) {
			throw new JOSEException("Ed25519Verifier requires a public key, use OctetKeyPair.toPublicJWK()");
		}

		this.publicKey = publicKey.toPublicKey();


		critPolicy.setDeferredCriticalHeaderParams(defCritHeaders);
	}


	/**
	 * Returns the public key.
	 *
	 * @return An OctetKeyPair without the private part
	 */
	public PublicKey getPublicKey() {

		return publicKey;
	}


	@Override
	public Set<String> getProcessedCriticalHeaderParams() {

		return critPolicy.getProcessedCriticalHeaderParams();
	}


	@Override
	public Set<String> getDeferredCriticalHeaderParams() {

		return critPolicy.getProcessedCriticalHeaderParams();
	}


	@Override
	public boolean verify(final JWSHeader header,
		              final byte[] signedContent, 
		              final Base64URL signature)
		throws JOSEException {

		// Check alg field in header
		final JWSAlgorithm alg = header.getAlgorithm();
		if (! JWSAlgorithm.EdDSA.equals(alg)) {
			throw new JOSEException("Ed25519Verifier requires alg=EdDSA in JWSHeader");
		}

		// Check for unrecognized "crit" properties
		if (! critPolicy.headerPasses(header)) {
			return false;
		}

		final byte[] jwsSignature = signature.decode();

		try {
			Signature sig = Signature.getInstance("EdDSA", BouncyCastleProviderSingleton.getInstance());
			sig.initVerify(publicKey);
			sig.update(signedContent);
			return sig.verify(signature.decode());

		} catch (InvalidKeyException | NoSuchAlgorithmException e) {
			throw new JOSEException("Invalid EC public key: " + e.getMessage(), e);
		} catch (SignatureException e) {
			return false;
		}
	}
}
